import maya.cmds as cmds 


# Pycharm - MayaCharm -------------------------------------------------
if not cmds.commandPort(':4434', query=True):
    cmds.commandPort(name=':4434')


# VScode - MayaPort -------------------------------------------------
if not cmds.commandPort(":7005", query=True):
    cmds.commandPort(name=":7005", sourceType="mel", echoOutput=True)

if not cmds.commandPort(":7007", query=True):
    cmds.commandPort(name=":7007", sourceType="python", echoOutput=True)


# Sublime Text-3 - MayaSublime -------------------------------------------------
if not cmds.commandPort(':7001', query=True):
    cmds.commandPort(name=":7001", sourceType="mel")

if not cmds.commandPort(':7002', query=True):
    cmds.commandPort(name=":7002", sourceType="python")


# save maya log
cmds.cmdFileOutput(o="D:/cmdFileOutput.log")
